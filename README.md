
## HighSpot Coding Challenge

Coding Exercise for Rob Young, 2020/02/24

The exercise reads "mixtape.json" and "changes.json", applies the changes in "changes.json", and writes the updated
mixtape objects to "output.json".

The changes supported are add a new playlist, remove a playlist, and add a song to a playlist.

### Running
Run from sbt with:

```
$ sbt run
```

### Acceptance Criteria:

- Must read a file "mixtape.json"
- Must read a "changes.json" file and apply the changes it contains
  - Add a new playlist
  - Verify the new playlist is non-empty
  - Remove a playlist
  - Add an existing song to an existing playlist
- Must write a file "output.json" if the input file is valid
- Must write "output.json" with all valid changes applied

### Scaling

_Add a README describing how you would scale this application to handle very large input files and/or very large changes files._

#### Scaling for large Mix Tapes
Obviously, persisting the mix tapes as a JSON file on disk isn't the best way to scale. I won't go into
re-architecting the solution to use a noSql or relational datastore. I'll focus on how to scale with the current
architecture.

There are several dimensions scaling could take:

##### Scaling for large Mix Tape files

- In this code I was good about following scala practice and using .copy() on immutable objects to change things,
but as the data grows, it would be faster to use mutable collections objects from "scala.collections.mutable" and
updating things in place.
- This implementation represents all the entities in application memory, but it might be worth considering loading
them in a simple memory store like Redis instead, or even a local in-memory database.

##### Scaling for large sets of changes

- JSON is a good format, but requires the entire file to be read before processing can start. Using a different format like
ndjson or csv could allow reading in the changes a line at a time and apply the changes concurrent with reading them.
- This implementation applies the changes serially, but I'd look at a way to apply the changes in parallel. This would
require a little thought about race conditions and conflicting operations.
For example a playlist being created and then deleted -- if the operations got processed out of order and the deletion
is handled first, then the playlist would not be deleted after all.
- The comments above about holding the entities in memory in immutable scala collections apply here, too. Each change to
a playlist requires a O(n) copy of the list being updated, so moving to mutable collections objects would speed things
up if there were many changes. But moving to a fast memory store or local database would help, too.

##### Scaling for large numbers of Users or Songs

- In this implementation, I only use sets of ids for users and songs to validate operations on playlists. These could be
represented more compactly as simple lists in the JSON, which would make the file smaller. The user or song data would
have to live somewhere else, of course.

##### Scaling for large numbers of mixtape or changes files

- To scale for load, processing huge numbers of mixtape or changes files, short of re-architecting the persistance, it
would be helpful to load mixtape files and hold them in memory to apply multiple changes against them. That
would save time reading from disk and parsing files repeatedly for multiple change files.

### Change File Format

- "changes.json" is a json file with a top-level properties:
  - "create_playlists"
    - a list of objects
        - each contains properties "user_id", "playlist_id" and "song_ids"
        - the new playlist_id will be the highest numbered playlist + 1
  - "delete_playlists"
    - list of delete objects
        - each contains the property "playlist_id"
  - "new_songs_for_playlists"
    - contains a list of songs to add to playlists
        - each contains properties "playlist_id" and "song_id"
- polymorphism is out of scope for this phase ;)

### Implementation Notes

- Will not check the internal consistency of "mixtape.json", e.g. repeated identifiers or invalid numbers for ids
- Changes are applied in order of
    - Create playlist
    - Add song to playlist
    - Delete playlist
- So you could add a playlist, add a song to it, then delete it in the same change file
- The change file identifies Playlists, Users, and Songs are identified
by their ID. Identifying them by name is out of scope for this phase. 
- Follow "MUST IGNORE" semantics, so do not write any unexpected properties in "mixtape.json" to "output.json". But
also don't fail if there are unexpected properties. This allows for forward-compatibility with future properties, even
if those properties are discarded by the current logic. But at least it won't fail.
- The program will attempt to write a correct file to output.json, containing the original playlists with any valid changes applied.
- Do not write "output.json" if the input file is invalid
- Write "output.json" with the original data if the changes file is invalid, or doesn't contain any valid changes.


### Error handling

- If the input file is invalid, the program will not write output.json, but will write an error message to stdout
- Write an error message to stdout if unable to write "output.json"
- Write an error message to stdout for each invalid change
- The program will write an error message to stdout when there is an operation is invalid, and ignore that instruction.
