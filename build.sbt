name := "highspot2"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.10.3"
libraryDependencies += "com.fasterxml.jackson.module" % "jackson-module-scala_2.12" % "2.10.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.7" % "test"

// libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.7"
// wartremoverErrors ++= Warts.unsafe
