package org.coding

import org.coding.MixTapeModel.{AddSongToPlaylist, CreatePlaylist, DeletePlaylist}

/**
 * Contains lists of changes to make
 *
 * @param createPlaylists    operations to create new playlists
 * @param deletePlaylists    operations to delete playlists
 * @param addSongToPlaylists operations to add a song to existing playlists
 */
final case class Changes(createPlaylists: Seq[CreatePlaylist],
                         deletePlaylists: Seq[DeletePlaylist],
                         addSongToPlaylists: Seq[AddSongToPlaylist]) {

  /**
   * Add all the new playlists
   *
   * @param mixTape the mixtape to update
   * @return the mixtape with valid playlists added
   */
  def add(mixTape: MixTape): MixTape = {
    // filter out invalid ones
    val validCreate = createPlaylists.filter(mixTape.validateCreatePlaylist)

    // zip together the list of new ids with the list of valid create operations
    val newAndId = (mixTape.maxPlaylistId + 1 to mixTape.maxPlaylistId + validCreate.size).zip(validCreate)

    // create all the new playlists, and add them in
    val newPlaylists = newAndId.map({ case (i, c) => c.toPlaylist(i.toString) })
    mixTape.copy(playlists = mixTape.playlists ++ newPlaylists)
  }

  /**
   * Add a song to a playlist.
   *
   * Has idempotent semantics; if the same song is added to the same playlist
   * more than once, it will only be added once
   */
  def addSongs(mixTape: MixTape): MixTape = {
    // filter out the invalid operations, and convert the add songs to a
    // map of playlist ID to song ID
    val validAdds = addSongToPlaylists
      .filter(mixTape.validateAddSongToPlaylist)
      .map(a => a.playlistId -> a.songId)
      .toMap

    // iterate through the list, and if the playlist is in our "add" list
    // and doesn't already contain the song, add it
    val playlists = for (playlist <- mixTape.playlists) yield ({
      validAdds.get(playlist.id) match {
        case Some(s) if ! playlist.songIds.contains(s) =>
          playlist.copy(songIds = playlist.songIds :+ s)
        case None =>
          playlist
      }
    })
    mixTape.copy(playlists = playlists)
  }

  /**
   * Delete playlists
   *
   * Has idempotent semantics; if the operation is to delete a playlist
   * ID that doesn't exist, we consider it deleted and drive on.
   *
   * @param mixTape the mixtape to update
   * @return the mixtape with playlists deleted
   */
  def delete(mixTape: MixTape): MixTape = {
    // If a playlist with a delete ID isn't present, we just treat it as already deleted

    // turn the list of operations into a list of playlists to delete
    val toDelete = deletePlaylists.map(_.playlistId).toSet

    // filter out the deleted playlists and rebuild the mixtape with the updated list
    val newPlaylists = mixTape.playlists.filter(p => !toDelete.contains(p.id))
    mixTape.copy(playlists = newPlaylists)
  }

  /**
   * Apply the changes to the mixtape
   *
   * @param mixTape0 the mixtape to update
   * @return the updated mixtape
   */
  def update(mixTape0: MixTape): MixTape = {
    val mixTape1 = add(mixTape0)
    val mixTape2 = addSongs(mixTape1)
    val mixTape3 = delete(mixTape2)
    mixTape3
  }
}
