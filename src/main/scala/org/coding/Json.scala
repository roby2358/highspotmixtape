package org.coding

import java.io.File

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper, PropertyNamingStrategy, SerializationFeature}
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import scala.util.Try

object Json {

  /**
   * This object mapper reads and writes properties from the JSON file in snake_case (lowercase with underscores),
   * and maps them to scala object properties in camelCase. E.g. user_id is mapped to userId.
   *
   * The object mapper also follows "MUST IGNORE" semantics, so unexpected properties are simply ignored.
   *
   * Ouput is indented for pretty formatting, although the extra whitespace does make the file larger.
   *
   * The scala module is loaded for interoperability with Scala case classes.
   *
   * @return an ObjectMapper with the right properties that interoperates with scala case classes
   */
  def snakeCaseObjectMapper = new ObjectMapper()
    .registerModule(DefaultScalaModule)
    .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    .enable(SerializationFeature.INDENT_OUTPUT)

  /**
   * Read mixtape from file, write to stdout if there's an error
   * @param fileName name of the file to read
   * @return mix tape object or none
   */
  def read[T](fileName: String, clazz: Class[T]): Option[T] = Try({
    val file = new File(fileName)
    val mixTape = Json.snakeCaseObjectMapper.readValue(file, clazz)
    Option(mixTape)
  })
    .recover({ case t: Throwable =>
      println(s"Could not read fileName=$fileName, error=${t.getClass.getSimpleName} ${t.getMessage}")
      None
    })
    .get
}
