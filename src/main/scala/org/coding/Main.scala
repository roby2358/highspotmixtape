package org.coding

import java.io.File

/**
 * The Main class has the main method to run from the command line
 *
 * Run this from the command line with "$ sbt run"
 */
object Main extends App {

  val inputFile = "mixtape.json"
  val outputFile = "output.json"
  val changesFile = "changes.json"

  val mixTape = Json.read(inputFile, classOf[MixTape])
  val changes = Json.read(changesFile, classOf[Changes])

  (mixTape, changes) match {
    case (None, _) =>
      println("Could not read mixtape.json")

    case (Some(mt), None) =>
      println("Could not read changes.json")

      // changes file was invalid, so output is just the original file unchanged
      Json.snakeCaseObjectMapper.writeValue(new File("output.json"), mt)

    case (Some(mt), Some(ch)) =>
      val output = ch.update(mt)
      
      println(output.users)
      println(output.songs)
      println(output.playlists)

      Json.snakeCaseObjectMapper.writeValue(new File("output.json"), output)
  }
}
