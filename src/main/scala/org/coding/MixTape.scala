package org.coding

import java.io.File

import com.fasterxml.jackson.annotation.JsonIgnore
import org.coding.MixTapeModel._

import scala.util.Try

/**
 * The companion object to MixTape has a utility function for creating a mix tape object
 */
object MixTape {

  /**
   * Read mixtape from file, write to stdout if there's an error
   *
   * @param fileName name of the file to read
   * @return mix tape object or none
   */
  def read(fileName: String): Option[MixTape] = Try({
    val file = new File(fileName)
    val mixTape = Json.snakeCaseObjectMapper.readValue(file, classOf[MixTape])
    Option(mixTape)
  })
    .recover({ case t: Throwable =>
      println(s"Could not load fileName=$fileName, error=${t.getClass.getSimpleName} ${t.getMessage}")
      None
    })
    .get
}

/**
 * Representation of a full mixtape file
 *
 * @param users     List of all the users
 * @param playlists List of all the playlists
 * @param songs     List of all songs available to the playlists
 */
final case class MixTape(users: Seq[User], playlists: Seq[Playlist], songs: Seq[Song]) {

  def update(changes: Changes): Option[MixTape] = {
    Option(this)
  }

  /**
   * The maxium ID of playlists
   */
  @JsonIgnore
  val maxPlaylistId: Int = playlists.map(_.id.toInt).max
  // TODO: handle cast exception

  /**
   * The set of valid user IDs
   */
  private val userIds: Set[String] = users.map(_.id).toSet

  /**
   * The set of valid song IDs
   */
  private val songIds: Set[String] = songs.map(_.id).toSet

  /**
   * Check a create playlist operation to see if it's valid
   *
   * @param createPlaylist the operation to check
   * @return true if valid
   */
  def validateCreatePlaylist(createPlaylist: CreatePlaylist): Boolean =
    if (!userIds.contains(createPlaylist.userId)) {
      // report error and return the original mixtape
      println(s"User does not exist userId=${createPlaylist.userId}")
      false
    }
    else if (!createPlaylist.songIds.forall(id => songIds.contains(id))) {
      // report error and return the original mixtape
      println(s"Song does not exist songIds=${createPlaylist.songIds}")
      false
    }
    else if (createPlaylist.songIds.isEmpty) {
      println(s"Playlist must contain at least one song")
      false
    }
    else
      true

  /**
   * Check an add song operation to see if it's valid
   *
   * @param addSongToPlaylist the operation to check
   * @return true if valid
   */
  def validateAddSongToPlaylist(addSongToPlaylist: AddSongToPlaylist): Boolean =
    if (!songIds.contains(addSongToPlaylist.songId)) {
      // report error and return the original mixtape
      println(s"Song does not exist songId=${addSongToPlaylist.songId}")
      false
    }
    else
      true
}
