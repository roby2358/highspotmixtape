package org.coding

/**
 * Contains classes for the domain data model.
 *
 * I might break out the operations from the entities in the next development round, but for now it's not too unmanagable.
 */
object MixTapeModel {

  /**
   * A user is a person who can have a playlist
   *
   * @param id Identifier for the user
   * @param name The person's name
   */
  final case class User(id: String, name: String)

  /**
   * A playlist associates a user with songs
   *
   * @param id Identifier for the playlist
   * @param userId ID of the person for this playlist
   * @param songIds A list of song IDs that are in the playlist
   */
  final case class Playlist(id: String, userId: String, songIds: Seq[String])

  /**
   * Identifies a song that can go in a playlist
   *
   * @param id Identifier for the song
   * @param artist The artist who performed the song
   * @param title Title of the song
   */
  final case class Song(id: String, artist: String, title: String)

  /**
   * An operation to create a new playlist
   *
   * @param userId Identifier of the playlist's user
   * @param songIds A list of song IDs for the playlist
   */
  final case class CreatePlaylist(userId: String, songIds: Seq[String]) {
    def toPlaylist(id: String): Playlist = Playlist(id = id, userId = userId, songIds = songIds)
  }

  /**
   * An operation to add a song to an existing playlist
   *
   * This operation is idempotent; if you add a song to a playlist that already contains that song, it won't be added again
   *
   * @param songId Identifier of the song to add
   * @param playlistId Identifier of the playlist to add it to
   */
  final case class AddSongToPlaylist(songId: String, playlistId: String)

  /**
   * An operation to delete a playlist
   *
   * This operation is idempotent; deleting a playlist that doesn't exist considers it already deleted
   *
   * @param playlistId Identifier of the playlist to delete
   */
  final case class DeletePlaylist(playlistId: String)
}
