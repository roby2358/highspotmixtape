package org.coding

import org.coding.MixTapeModel.{CreatePlaylist, Playlist, Song, User}
import org.scalatest.{FlatSpec, MustMatchers}

/**
 * Test case to provide some coverage of the logic. More coverage is needed.
 */
class MixTapeSpec extends FlatSpec with MustMatchers {

  private val anyUsers = Seq(User("1111", "Someone"))
  private val anySongs = Seq(Song("2222", "Pink Floyd", "Shine on You Crazy Diamond"))
  private val anyPlaylists = Seq(Playlist("3333", "1111", Seq("2222")))
  private val anyMixTape = MixTape(anyUsers, anyPlaylists, anySongs)

  "validateCreatePlaylist" must "reject a bad user id" in {
    val badUser = CreatePlaylist("X", Seq("2222"))
    anyMixTape.validateCreatePlaylist(badUser) mustBe false
  }

  it must "reject a bad song id" in {
    val badSong = CreatePlaylist("1111", Seq("X"))
    anyMixTape.validateCreatePlaylist(badSong) mustBe false
  }

  it must "ok a good one" in {
    val good = CreatePlaylist("1111", Seq("2222"))
    anyMixTape.validateCreatePlaylist(good) mustBe true
  }

  // TODO: everything else
}
